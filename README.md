## 功能介绍 
    
剧本杀预约小程序是专门针对剧本杀门店开发的，具有预约，剧本展示，小店动态等功能。z世代的年轻人兴趣爱好广泛，尤其是游戏依旧是主流年轻人的喜好。除了追星，直播以外，剧本杀成为了当下热门的线下社交活动

- 预约管理：开始/截止时间/人数均可灵活设置，可以自定义客户预约填写的数据项
- 预约凭证：支持线下到场后校验签到/核销/二维码自助签到等多种方式
- 详尽的预约数据：支持预约名单数据导出Excel，打印

 ![输入图片说明](%E4%BA%8C%E7%BB%B4%E7%A0%81.png)

## 技术运用
- 本项目使用微信小程序平台进行开发。
- 使用腾讯专门的小程序云开发技术，云资源包含云函数，数据库，带宽，存储空间，定时器等，资源配额价格低廉，无需域名和服务器即可搭建。
- 小程序本身的即用即走，适合小工具的使用场景，也适合快速开发迭代。
- 云开发技术采用腾讯内部链路，没有被黑客攻击的风险，安全性高且免维护。
- 资源承载力可根据业务发展需要随时弹性扩展。  



## 作者
- 如有疑问，欢迎骚扰联系我鸭：开发交流，技术分享，问题答疑，功能建议收集，版本更新通知，安装部署协助，小程序开发定制等。
- 俺的微信:

 ![输入图片说明](demo/author-base.png)



## 演示

  ![输入图片说明](%E4%BA%8C%E7%BB%B4%E7%A0%81.png)

 

## 安装

- 安装手册见源码包里的word文档




## 截图
 ![输入图片说明](demo/%E6%A0%91%E5%8F%B6.png)

![输入图片说明](demo/%E5%89%A7%E6%9C%AC%E6%95%85%E4%BA%8B.png)

![输入图片说明](demo/%E5%89%A7%E6%9C%AC%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/%E6%88%91%E7%9A%84.png)
![输入图片说明](demo/%E5%B0%8F%E5%BA%97%E5%8A%A8%E6%80%81.png)
![输入图片说明](demo/%E9%A2%84%E7%BA%A6.png)
![输入图片说明](demo/%E9%A2%84%E7%BA%A6%E6%88%90%E5%8A%9F.png)
![输入图片说明](demo/%E9%A2%84%E7%BA%A6%E6%97%A5%E5%8E%86.png)
![输入图片说明](demo/%E9%A2%84%E7%BA%A6%E6%80%A7%E6%83%85.png)


## 后台管理系统截图
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E7%AE%A1%E7%90%86.png)
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E8%8F%9C%E5%8D%95.png)
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E5%90%8D%E5%8D%95%E5%AF%BC%E5%87%BA.png)
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E5%90%8D%E5%8D%95.png)

![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E6%97%B6%E6%AE%B5.png)
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E9%A2%84%E7%BA%A6%E6%B7%BB%E5%8A%A0.png)
![输入图片说明](demo/%E5%90%8E%E5%8F%B0-%E6%A0%B8%E9%94%80.png)