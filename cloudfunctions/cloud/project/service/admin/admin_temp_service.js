/**
 * Notes: 预约后台管理
 * Ver : CCMiniCloud Framework 2.0.1 ALL RIGHTS RESERVED BY cclinux@qq.com
 * Date: 2021-12-08 07:48:00 
 */

const BaseAdminService = require('./base_admin_service.js');
const TempModel = require('../../model/temp_model.js');
const dataUtil = require('../../../framework/utils/data_util.js');

class AdminTempService extends BaseAdminService {

	/**添加模板 */
	async insertTemp({
		name,
		times,
	}) {
    let data = {
      "TEMP_NAME": name,
      "TEMP_TIMES": times
    }
    return await TempModel.insert(data);
	}

	/**更新数据 */
	async editTemp({
		id,
		limit,
		isLimit
	}) {
    let where = {
      "_id": id
    }
    let temp = await TempModel.getOne(where);
    let times = dataUtil.deepClone(temp.TEMP_TIMES);
    times.forEach(time => {
      time.isLimit = isLimit;
      time.limit = limit;
    }) 
    let data = {
      "TEMP_TIMES": times
    }
    return await TempModel.edit(where,data);
	}


	/**删除数据 */
	async delTemp(id) {
		let where = {
      "_id": id
    }
    return await TempModel.del(where);
	}


	/**分页列表 */
	async getTempList() {
		let orderBy = {
			'TEMP_ADD_TIME': 'desc'
		};
		let fields = 'TEMP_NAME,TEMP_TIMES';

		let where = {};
		return await TempModel.getAll(where, fields, orderBy);
	}
}

module.exports = AdminTempService;