/**
 * Notes: 预约后台管理
 * Ver : CCMiniCloud Framework 2.0.1 ALL RIGHTS RESERVED BY www.code3721.com
 * Date: 2022-12-08 07:48:00 
 */

const BaseAdminService = require('./base_admin_service.js');
const timeUtil = require('../../../framework/utils/time_util.js');

const MeetModel = require('../../model/meet_model.js');
const JoinModel = require('../../model/join_model.js');
const UserModel = require('../../model/user_model.js');

const DataService = require('./../data_service');
const { stackTraceLimit } = require('../../../framework/core/app_error.js');

// 导出报名数据KEY
const EXPORT_JOIN_DATA_KEY = 'join_data';

// 导出用户数据KEY
const EXPORT_USER_DATA_KEY = 'user_data';

class AdminExportService extends BaseAdminService {
	// #####################导出报名数据
	/**获取报名数据 */
	async getJoinDataURL() {
		let dataService = new DataService();
		return await dataService.getExportDataURL(EXPORT_JOIN_DATA_KEY);
	}

	/**删除报名数据 */
	async deleteJoinDataExcel() {
		let dataService = new DataService();
		return await dataService.deleteDataExcel(EXPORT_JOIN_DATA_KEY);
	}

	// 根据表单提取数据
	_getValByForm(arr, mark, title) {
		for (let k in arr) {
			if (arr[k].mark == mark) return arr[k].val;
			if (arr[k].title == title) return arr[k].val;
		}

		return '';
	}

	/**导出报名数据 */
	async exportJoinDataExcel({
		meetId,
		startDay,
		endDay,
		status
	}) {
    let where = {
      "JOIN_MEET_ID": meetId,
      "JOIN_STATUS": status
    }
    //截取日期
    if(startDay&&endDay){
      where.JOIN_MEET_DAY = ['between', startDay, endDay];
    }else{
      this.AppError('起始时间和开始时间不能为空');
    }
    let joinDataList = await JoinModel.getAll(where);
    if(joinDataList.length > 0){
      let formList = joinDataList[0].JOIN_FORMS;
      let head = ["活动标题","活动预约日期","开始时间","结束时间","用户预约时间"];
      formList.forEach(form =>  head.push(form.title))
      let data = [head];
      joinDataList.forEach(joinData => {
        formList = joinData.JOIN_FORMS
        let excelData = [
          joinData.JOIN_MEET_TITLE,
          joinData.JOIN_MEET_DAY,
          joinData.JOIN_MEET_TIME_START,
          joinData.JOIN_MEET_TIME_END,
          timeUtil.timestamp2Time(joinData.JOIN_ADD_TIME)
        ];
        formList.forEach(form => {
          excelData.push(form.val);
        });
        data.push(excelData);
      })
      let dataService = new DataService();
      return dataService.exportDataExcel(EXPORT_JOIN_DATA_KEY,"报名信息",joinDataList.length,data);
    }else{
      this.AppError('暂无报名数据')
    }
	}


	// #####################导出用户数据

	/**获取用户数据 */
	async getUserDataURL() {
		let dataService = new DataService();
		return await dataService.getExportDataURL(EXPORT_USER_DATA_KEY);
	}

	/**删除用户数据 */
	async deleteUserDataExcel() {
		let dataService = new DataService();
		return await dataService.deleteDataExcel(EXPORT_USER_DATA_KEY);
	}

	/**导出用户数据 */
	async exportUserDataExcel(condition) {
    let and = decodeURIComponent(condition);
    let where = JSON.parse(and).and;
    let userDataList = await UserModel.getAll(where);
    let head = ['姓名','手机号','所在城市']
    let data = [head];
    userDataList.forEach(user => {
      let excelData = [user.USER_NAME,user.USER_MOBILE,user.USER_CITY];
      data.push(excelData);
    });
    let dataService = new DataService();
    return dataService.exportDataExcel(EXPORT_USER_DATA_KEY, "用户信息", userDataList.length, data)
	}
}

module.exports = AdminExportService;